package com.nx.arch.transactionmsg.utils;

/**
 * @类名称 Config.java
 * @类描述 配置信息对象
 * @作者  庄梦蝶殇 linhuaichuan@naixuejiaoyu.com
 * @创建时间 2020年4月14日 下午2:41:59
 * @版本 1.0.0
 *
 * @修改记录
 * <pre>
 *     版本                       修改人 		修改日期 		 修改内容描述
 *     ----------------------------------------------
 *     1.0.0 		庄梦蝶殇 	2020年4月14日             
 *     ----------------------------------------------
 * </pre>
 */
public class Config {
    /**
     * 事务消息-工作线程数
     */
    public int threadNum = 10;
    
    /**
     * 180s扫描一次表，删除历史消息
     */
    public int deleteTimePeriod = 180;
    
    /**
     * 一次删除200条消息
     */
    public int deleteMsgOneTimeNum = 200;
    
    public int schedThreadNum = 6;
    
    public int minIdleConnectionNum = 6;
    
    public int maxActiveConnectionNum = 20;
    
    public int sendMsgTimeout = 600;
    
    /**
     * 120s扫描一次，待发送的继续发送
     */
    public int schedScanTimePeriod = 120;
    
    /**
     * mysql 连接池，拿连接的等待时间
     */
    public int maxWaitTime = 6000;
    
    public int closeWaitTime = 5000;
    
    /**
     *  2分钟一次输出内部queue堆积的数量
     */
    public int statsTimePeriod = 120;
    
    public int historyMsgStoreTime = 3;
    
    public String msgTableName = "mq_messages";
    
    public String[] etcdHosts;
    
    public String serviceName = "transmsg";
    
    public Config() {
        
    }
    
    public int getThreadNum() {
        return threadNum;
    }
    
    /**
     * 
     * @param threadNum 消息处理线程数，默认为10
     */
    public void setThreadNum(int threadNum) {
        this.threadNum = threadNum;
    }
    
    public int getDeleteTimePeriod() {
        return deleteTimePeriod;
    }
    
    /**
     * 
     * @param deleteTimePeriod 删除历史消息的周期，默认180s
     */
    public void setDeleteTimePeriod(int deleteTimePeriod) {
        this.deleteTimePeriod = deleteTimePeriod;
    }
    
    public int getDeleteMsgOneTimeNum() {
        return deleteMsgOneTimeNum;
    }
    
    /**
     * 
     * @param deleteMsgOneTimEnum 一次删除历史消息的条数，默认为200
     */
    public void setDeleteMsgOneTimeNum(int deleteMsgOneTimEnum) {
        this.deleteMsgOneTimeNum = deleteMsgOneTimEnum;
    }
    
    public int getSchedThreadNum() {
        return schedThreadNum;
    }
    
    /**
     * 
     * @param schedThreadNum 内部定时的线程数，默认为6
     */
    public void setSchedThreadNum(int schedThreadNum) {
        this.schedThreadNum = schedThreadNum;
    }
    
    public int getMinIdleConnectionNum() {
        return minIdleConnectionNum;
    }
    
    /**
     * @param minIdleConnectionNum mysql 最少连接数，默认为6
     */
    public void setMinIdleConnectionNum(int minIdleConnectionNum) {
        this.minIdleConnectionNum = minIdleConnectionNum;
    }
    
    public int getMaxActiveConnectionNum() {
        return maxActiveConnectionNum;
    }
    
    /**
     * 
     * @param maxActiveConnectionNum mysql 最大活跃连接数，默认为20
     */
    public void setMaxActiveConnectionNum(int maxActiveConnectionNum) {
        this.maxActiveConnectionNum = maxActiveConnectionNum;
    }
    
    public int getSendMsgTimeout() {
        return sendMsgTimeout;
    }
    
    /**
     * 
     * @param sendMsgTimeout 内部producer发送超时时间
     */
    public void setSendMsgTimeout(int sendMsgTimeout) {
        this.sendMsgTimeout = sendMsgTimeout;
    }
    
    public int getSchedScanTimePeriod() {
        return schedScanTimePeriod;
    }
    
    /**
     * @param schedScanTimePeriod 定时扫描周期，默认为120S
     */
    public void setSchedScanTimePeriod(int schedScanTimePeriod) {
        this.schedScanTimePeriod = schedScanTimePeriod;
    }
    
    public int getMaxWaitTime() {
        return maxWaitTime;
    }
    
    public void setMaxWaitTime(int maxWaitTime) {
        this.maxWaitTime = maxWaitTime;
    }
    
    public int getCloseWaitTime() {
        return closeWaitTime;
    }
    
    public void setCloseWaitTime(int closeWaitTime) {
        this.closeWaitTime = closeWaitTime;
    }
    
    public int getStatsTimePeriod() {
        return statsTimePeriod;
    }
    
    public void setStatsTimePeriod(int statsTimePeriod) {
        this.statsTimePeriod = statsTimePeriod;
    }
    
    public int getHistoryMsgStoreTime() {
        return historyMsgStoreTime;
    }
    
    /**
     * 
     * @param historyMsgStoreTime 历史消息默认保存三天
     */
    public void setHistoryMsgStoreTime(int historyMsgStoreTime) {
        this.historyMsgStoreTime = historyMsgStoreTime;
    }
    
    public String getMsgTableName() {
        return msgTableName;
    }
    
    public void setMsgTableName(String msgTableName) {
        this.msgTableName = msgTableName;
    }
    
    public String[] getEtcdHosts() {
        return etcdHosts;
    }
    
    /**
     * @param etcdHosts 需要带http前缀的
     */
    public void setEtcdHosts(String[] etcdHosts) {
        this.etcdHosts = etcdHosts;
    }
    
    public String getServiceName() {
        return serviceName;
    }
    
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    
}
